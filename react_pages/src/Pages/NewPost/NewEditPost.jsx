import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router';

const NewEditPost = () => {
    const [ post, setPost ] = useState({ text: '', title: '' });

    const navigate = useNavigate();
    const { newsId } = useParams();

    useEffect(() => {
        if(newsId) {
            const fetchData = async () => {
                try {
                  const { data } = await axios.get(`http://localhost:8000/api/newsposts/${newsId}`);
                  const { text, title } = data;
                  setPost({text, title})
                } catch (error) {
                  console.error('Error fetching data:', error);
                }
              };
          
              fetchData();
        }
    }, [newsId]);

    const handleTitleChange = (e) => {
        setPost({ ...post, title: e.target.value})
    };

    const handleTextChange = (e) => {
        setPost({ ...post, text: e.target.value})
    };

    const addEditPost = async (event) => {
        event.preventDefault();
        if(newsId) {
            await axios.put(`http://localhost:8000/api/newsposts/${newsId}`, post);
            navigate('/');
        } else {
            await axios.post('http://localhost:8000/api/newsposts', post);
            navigate('/');
        };
    };

    return (
        <form
            style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                gap: '30px'
            }}
        >
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                gap: '30px'
            }}>
                <label htmlFor='title' >Enter title</label>
                <input style={{ width: '500px', height: '50px', textAlign: 'center' }} value={post.title} onChange={handleTitleChange} id='title'/>
            </div>
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                gap: '30px'
            }}>
                <label htmlFor='text' >Enter text</label>
                <input style={{ width: '500px', height: '50px', textAlign: 'center' }} value={post.text} onChange={handleTextChange} id='text'/>
            </div>
            <button type='submit' onClick={addEditPost} style={{ padding: '10px', backgroundColor: 'light-green' }}>Submit</button>
        </form>
    )
}

export default NewEditPost
