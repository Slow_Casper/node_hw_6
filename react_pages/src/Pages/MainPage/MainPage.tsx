import React, { useEffect, useState } from 'react';
import axios from 'axios';
import PostsList from '../../Components/Posts/PostsList/PostsList.tsx';
import Button from '../../Components/Button/Button.tsx';
import { NavLink } from 'react-router-dom';

const MainPage = () => {
  const [ posts, setPosts ] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get('http://localhost:8000/api/newsposts');
        setPosts(data)
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [])

  return (
    <section
      style={{
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        gap: '20px'
      }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          gap: '20px',
        }}
      >
        <h1>Welcome to the actual news</h1>
        <NavLink
          to='/newpost'
          style={{
            backgroundColor: 'gray',
            padding: '10px',
            textDecoration: 'none',
            color: 'black',
          }}
        >
          Add post
        </NavLink>
      </div>
      <PostsList posts={posts} />
    </section>
  );
};

export default MainPage;
