const fsPromise = require('fs/promises');
const path = require('path');

const newsDBPath = path.join(__dirname, '../Files', 'news.json');

exports.getAllPosts = async (req, res) => {
    try {
        const access = await fsPromise.access(newsDBPath).then(() => true).catch(() => false);

        if(access) {
            const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
            res.status(200).send(newsDB);
        } else {
            await fsPromise.writeFile(newsDBPath, '[]');
            const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
            console.log(newsDB)
            res.status(400).send(newsDB);
        }
    } catch (error) {
        res.status(500).json({
            message: `Error happened on server: "${error}`
        });
    };
};

exports.addNewPost = async (req, res) => {
    const id = Date.now();
    const { text, title } = req.body

    const newPost = { id, title, text };
    try {
        const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
        const parsedNews = JSON.parse(newsDB);
        
        parsedNews.push(newPost);
        
        await fsPromise.writeFile(newsDBPath, JSON.stringify(parsedNews, null, 2));
        res.status(200).json(parsedNews);
    } catch (error) {
        res.status(500).json({
            message: `Error happened on server: "${error}`,
        });
    };
};

exports.getSinglePost = async (req, res) => {
    try {
        const id = req.params.id
        const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
        const parsedNews = JSON.parse(newsDB);
    
        const currentPost = parsedNews.find((el) => el.id == id)
        if (currentPost) {        
            res.status(200).send(currentPost);
        } else {
            res.status(400).json({
                message: `Post with id ${id} no found`,
            });
        };
    } catch (error) {
        res.status(500).json({
            message: `Error happened on server: "${error}`
        });
    }
};

exports.updatePost = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        
        const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
        const parsedNews = JSON.parse(newsDB);
        
        const currentPost = parsedNews.find((el) => el.id == id);
        if(currentPost) {
            Object.keys(body).forEach(key => currentPost[key] = body[key]);
            await fsPromise.writeFile(newsDBPath, JSON.stringify(parsedNews, null, 2));
            res.status(200).json({
                success: true,
                post: currentPost,
            });
        } else {
            res.status(400).json({
                message: `Post with id ${id} no found`,
            });
        }
    } catch (error) {
        res.status(500).json({
            message: `Error happened on server: "${error}`
        });
    }
};

exports.deletePost = async (req, res) => {
    const id = req.params.id;

    const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
    const parsedNews = JSON.parse(newsDB);

    const filteredPosts = parsedNews.filter((el) => el.id != id);
    if (filteredPosts) {
        try {
            await fsPromise.writeFile(newsDBPath, JSON.stringify(filteredPosts, null, 2));
            res.status(200).json({
                success: true,
                post: filteredPosts,
            });
        } catch (error) {
            res.status(500).json({
                message: `Error happened on server: "${error}`
            });
        };
    } else {
        res.status(400).json({
            message: `Post with id ${id} no found`,
        });
    };
};
