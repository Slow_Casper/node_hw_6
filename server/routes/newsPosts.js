const fsPromise = require('fs/promises');
const express = require('express');
const path = require('path');
const { getAllPosts, getSinglePost, addNewPost, updatePost, deletePost } = require('../controllers/posts');
const router = express.Router();

const newsDBPath = path.join(__dirname, '../Files', 'news.json');

router.get('/', getAllPosts);

router.post('/', addNewPost);

router.get('/:id', getSinglePost);

router.put('/:id', updatePost);

router.delete('/:id', deletePost);

module.exports = router;
