const express = require("express");
const router = express.Router();

const { staticGet } = require('../controllers/staticGetController');

router.get('/', staticGet);

module.exports = router;
